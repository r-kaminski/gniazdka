import javax.swing.*;

public class ClientMain {
    public static void main(String[] args) {
        while (true) {
            ClientApp client = new ClientApp();
            client.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            client.setSize(480, 480);
            client.setTitle("Tic Tac Toe");
            client.setVisible(true);
            client.setResizable(false);
            int result = client.startGame();
            if (client.showResultDialog(result) != 0) {
                client.dispose();
                break;
            }
            client.dispose();
        }



    }
}
