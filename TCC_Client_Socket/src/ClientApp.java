import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.LinkedList;

public class ClientApp extends JFrame {

    private JPanel contentPane;

    private final int NUM_TILES = 9;

    private JPanel[] tiles = new JPanel[NUM_TILES];
    private JLabel[] icons = new JLabel[NUM_TILES];

    private ImageIcon myIcon;
    private ImageIcon enemyIcon;

    private Socket socket;
    private DataInputStream input;
    private DataOutputStream output;

    private LinkedList<Integer> tile_num = new LinkedList<>();

    private Object[] options = {"Yes", "No, thanks"};


    public ClientApp() {
        try {
            socket = new Socket("localhost", 5000);
            input = new DataInputStream(socket.getInputStream());
            output = new DataOutputStream(socket.getOutputStream());

        } catch (IOException e) {
            System.out.println("Failed to connect");
            System.exit(0);
        }


        contentPane = new JPanel(new GridLayout(3, 3));

        for (int i = 0; i < 9; i++) {
            tiles[i] = new JPanel();
            //rgb(83, 91, 104)
            //217 229 247
            tiles[i].setBackground(new Color(83, 91, 104));
            tiles[i].setBorder(BorderFactory.createLineBorder(Color.BLACK));
            final int TMP_I = i;

            icons[i] = new JLabel((Icon)null);

            tiles[i].add(icons[i]);
            contentPane.add(tiles[i]);
        }
        getContentPane().add(contentPane, "Center");
    }


    public void setIcon(Icon icon, int i) {
        icons[i].setIcon(icon);
    }

    public int showResultDialog(int result) {

        //TODO
        String gameResult = "";
        if (result == 100) {
            gameResult = "won";
        } else if (result == 101) {
            gameResult = "lost";
        } else if (result == 102) {
            gameResult = "drew";
        }

        return JOptionPane.showOptionDialog(contentPane, "You " + gameResult +"! Want to rematch?", "Game Over", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE,
                null, options, options[0]);

    }

    public int startGame() {
        int response;
        try {


            //Assigning X/O to each player
            response = input.readInt();
            if(response == 200) {
                myIcon = new ImageIcon("x.png");
                enemyIcon = new ImageIcon("o.png");
            } else if (response == 201) {
                myIcon = new ImageIcon("o.png");
                enemyIcon = new ImageIcon("x.png");
            }
            for (int i = 0; i < 9; i++) {
                final int TMP_I = i;
                tiles[i].addMouseListener(new MouseAdapter() {
                    @Override
                    public void mousePressed(MouseEvent e) {
                        tile_num.addFirst(TMP_I);
                        try {
                            output.writeInt(TMP_I);
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    }
                });
            }

            while(true) {
                response = input.readInt();
                if (response == 10) {
                    response = input.readInt();
                    if(response == 99) {
                        setIcon(myIcon, tile_num.getFirst());
                    } else if (response == 100) {
                        setIcon(myIcon, tile_num.getFirst());
                        return 100;

                    } else if (response == 102) {
                        setIcon(myIcon, tile_num.getFirst());
                        return 102;
                    }
                } else if(response >= 300 && response < 400){
                    setIcon(enemyIcon, response - 300);
                } else if (response == 11){
                } else if (response == 101) {
                    return 101;
                } else if (response >= 400) {
                    System.out.println("Inside 400");
                    setIcon(enemyIcon, response - 400);
                    return 102;
                }

            }


        } catch(IOException e) {
            e.printStackTrace();
        }
        return 0;
    }


}
