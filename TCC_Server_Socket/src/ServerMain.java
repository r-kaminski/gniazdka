import javax.swing.*;

public class ServerMain {
    public static void main(String[] args) {
        /*
        0 - start
        1-9 - tiles
        10 - valid move
        11 - invalid move

        98 - enemy moved
        99 - nothing happens
        100 - victory
        101 - defeat
        102 - draw


        200 - x
        201 - o
        */
        ServerApp app = new ServerApp();
        app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        app.setSize(400, 250);
        app.setTitle("Tic Tac Toe");
        app.setVisible(true);
        app.startServer();
    }
}
