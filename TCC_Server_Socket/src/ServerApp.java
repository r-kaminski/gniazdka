
import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

public class ServerApp extends JFrame {
    private ServerSocket serverSocket;

    private static final long serialVersionUID = 1L;

    private JScrollPane scroll;
    private JTextArea information;
    private JLabel title;

    int sessionNo;


    public ServerApp() {

        BorderLayout layout = new BorderLayout();
        setLayout(layout);
        title = new JLabel("Server");
        information = new JTextArea();
        scroll = new JScrollPane(information);

        information.setBackground(new Color(83,91,104));
        information.setForeground(Color.WHITE);

        add(title, BorderLayout.NORTH);
        add(scroll, BorderLayout.CENTER);


    }

    public void startServer() {
        try {
                serverSocket = new ServerSocket(5000);
                information.append(serverSocket.getInetAddress().getHostAddress());
                information.append(new Date() + ":- Server start at port 5000\n");

                sessionNo = 1;


                while(true) {

                    Socket socket1 = serverSocket.accept();

                    information.append(new Date()+ ":- Session "+ sessionNo + " has started\n");
                    information.append(new Date() + ":- player1 joined at ");
                    information.append(socket1.getInetAddress().getHostAddress() + "\n");


                    Socket socket2 = serverSocket.accept();
                    information.append(new Date() + ":- player2 joined at ");
                    information.append(socket2.getInetAddress().getHostAddress() +"\n");

                    sessionNo++;
                    PlayerApp playerApp = new PlayerApp(socket1, socket2);
                }

        } catch(IOException e) {
            System.out.println("Encountered error when developing client connection");
            System.exit(0);
        }
    }


}
