import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class PlayerApp {

    private Player player1;
    private Player player2;
    private Player activePlayer;


    private Player[] tiles = {
            null, null, null, null, null, null, null, null, null
    };

    public PlayerApp(Socket s1, Socket s2) {
        player1 = new Player(s1, 'x');
        player2 = new Player(s2, 'o');
        activePlayer = player1;

        player1.setEnemyPlayer(player2);
        player2.setEnemyPlayer(player1);

        player1.start();
        player2.start();

    }

    public synchronized boolean isValid(Player player, int tile) {
        if (tile < 9 && tile >= 0) {
            if(activePlayer == player && tiles[tile] == null) {
                tiles[tile] = activePlayer;
                activePlayer = activePlayer.getEnemyPlayer();
                return true;
            }
        }
        return false;
    }

    public int checkForWinner() {
        if (tiles[0] != null && tiles[0] == tiles[1] && tiles[0] == tiles[2]
                || tiles[3] != null && tiles[3] == tiles[4] && tiles[3] == tiles[5]
                || tiles[6] != null && tiles[6] == tiles[7] && tiles[6] == tiles[8]
                || tiles[1] != null && tiles[1] == tiles[4] && tiles[1] == tiles[7]
                || tiles[0] != null && tiles[0] == tiles[3] && tiles[0] == tiles[6]
                || tiles[2] != null && tiles[2] == tiles[4] && tiles[2] == tiles[6]
                || tiles[0] != null && tiles[0] == tiles[4] && tiles[0] == tiles[8]
                || tiles[2] != null && tiles[2] == tiles[5] && tiles[2] == tiles[8]) {
            return 100;
        }

        for(int i = 0; i < 9; i++) {
            if(tiles[i] == null) {
                return 99;
            }
        }
        return 102;



    }


    public class Player extends Thread {

        private Socket socket;
        private Player enemyPlayer;

        public char sign;
        private DataInputStream input;
        private DataOutputStream output;

        public Player(Socket socket, char sign) {
            this.socket = socket;
            this.sign = sign;
            try {
                input = new DataInputStream(socket.getInputStream());
                output = new DataOutputStream(socket.getOutputStream());
            } catch(IOException e) {
                System.out.println("Error when initializing data streams");
                e.printStackTrace();
            }
        }


        public DataOutputStream getOutput() {
            return this.output;
        }

        public void setEnemyPlayer(Player enemyPlayer) {
            this.enemyPlayer = enemyPlayer;
        }







        public Player getEnemyPlayer() {
            return this.enemyPlayer;
        }
        @Override
        public void run() {
            try {

                if(sign == 'x') {
                    output.writeInt(200);
                } else if (sign == 'o') {
                    output.writeInt(201);
                } else {
                    System.out.println("Error while assigning X/O");
                    System.exit(0);
                }

                while(true) {
                    int response = input.readInt();
                    if (isValid(this, response)) {
                        output.writeInt(10); //valid move
                        int winResponse = checkForWinner();
                        if (winResponse == 100) {
                            output.writeInt(winResponse); //message about winning
                            enemyPlayer.getOutput().writeInt(101); //message about losing
                        } else if (winResponse == 102){
                            output.writeInt(winResponse); //message about draw
                            enemyPlayer.getOutput().writeInt(400 + response); //message about draw
                        } else if (winResponse == 99) {
                            output.writeInt(winResponse);
                            enemyPlayer.getOutput().writeInt(300 + response);
                        }
                    } else {
                        output.writeInt(11); //invalid move
                    }

                }
            } catch(IOException e) {
                System.out.println("Player left the server");
            } finally {
                try {
                    socket.close();
                } catch(IOException e) {
                    e.printStackTrace();
                }
            }
        }





    }

}
